package com.kulaev.spring.mvc_hibernate_aop.dao;

import com.kulaev.spring.mvc_hibernate_aop.Entity.Employee;

import java.util.List;

public interface EmployeeDAO {
    public List<Employee> getAllEmployees();
    public void saveEmployee(Employee employee);

    public Employee getEmployee(int id);

    public void deleteEmployee(int id);
}
