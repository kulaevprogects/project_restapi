package com.kulaev.spring.mvc_hibernate_aop.service;

import com.kulaev.spring.mvc_hibernate_aop.Entity.Employee;

import java.util.List;

public interface EmployeeService {
    public List<Employee> getAllEmployees();
    public void saveEmployee(Employee employee);
    public Employee getEmployee(int id);
    public void deleteEmployee(int id);
}
