package com.kulaev.spring.mvc_hibernate_aop.controllers;

import com.kulaev.spring.mvc_hibernate_aop.Entity.Employee;
import com.kulaev.spring.mvc_hibernate_aop.dao.EmployeeDAO;
import com.kulaev.spring.mvc_hibernate_aop.dao.EmployeeDAOImplemetation;
import com.kulaev.spring.mvc_hibernate_aop.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class MyController {
    @Autowired
    private EmployeeService employeeService;
    @RequestMapping("/")
    public String showAllEmployees(Model model){
        List<Employee> employees = employeeService.getAllEmployees();
        model.addAttribute("allEmployees",employees);
        return "all-employees";
    }
    @RequestMapping("/addNewEmployee")
    public String addNewEmloyee(Model model){
        Employee employee = new Employee();
        model.addAttribute("employee",employee);
        return "employee-info";
    }
    @RequestMapping("/saveEmployee")
    public String saveEmployee(@ModelAttribute("employee") Employee employee){
        employeeService.saveEmployee(employee);
        return "redirect:/";
    }
    @RequestMapping("/updateInfo")
    public String updateEmployee(@RequestParam("employeeId")int id, Model model){
        Employee employee = employeeService.getEmployee(id);
        System.out.println(employee);
        model.addAttribute("employee",employee);
        return "employee-info";
    }
    @RequestMapping("/deleteEmployee")
    public String deleteEmployee(@RequestParam("employeeId")int id){
        employeeService.deleteEmployee(id);
        return "redirect:/";
    }
}
